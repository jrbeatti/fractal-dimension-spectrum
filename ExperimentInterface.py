# Experimental Interface for Calculating the Fractal Dimension
# Updated: 11 April, 2018
# Author: James Beattie

################################################################################################################

import os;
import subprocess;
import re;
import numpy as np;
import pandas as pd;
import matplotlib.pyplot as plt;
import argparse;

##################################################################################################################################################

ap 			= argparse.ArgumentParser(description = 'Just a bunch of input arguments');
ap.add_argument('-peaks','--peaks',required=False,help='the number of peaks in the segmented mass length methods',type=int,default=1);
ap.add_argument('-filepath','--filepath',required=True,help='the directory where the files live',type=str);
ap.add_argument('-writepath','--writepath',required=True,help='the directory where the .csv files are written to',type=str);
ap.add_argument('-centers','--centers',required=True,help='the method for the centers: RegionCentroid, RegionMaxpixel, Maxpixel',type=str);
ap.add_argument('-readtype','--readtype',required=True,help='the method for the reading the h5py files: all, projection, slice ',type=str);
args 		= vars(ap.parse_args());

##################################################################################################################################################

file_PATH   = args['filepath']
write_PATH  = args['writepath']

os.chdir(file_PATH)
os.system('ls EXTREME* ' + '> Data.txt')

######## Mass Length Experiment ########

# The iteration counter through each of the files
iter_counter    = 0;

# The number of files initiation.
num_files       = 0;

f = open('Data.txt','r');
for line in f: num_files +=1;

f = open('Data.txt','r');
for line in f:

    # define the file
    file_ = line.strip('\n');

    box_count_EXP   = [];   # intitialise the amount of boxes
    mass_EXP        = [];   # intitliase the mass
    fractal_dim_EXP = [];   # initialise the fractal dimension
    length_EXP      = [];   # intialise the size of the boxes
    max_peak        = [];   # intialise the maximum number of peaks
    time_step       = [];   # intialise the timestep
    sd_EXP          = [];   # initialise standard deviation
    skips           = ['Data Skipped:']; # initialise skip counter

    # Use subprocess to call the FractalDimensionCalculation script.
    proc            = subprocess.Popen("python /Volumes/JamesBe/MasterScript/FractalDimensionCalculation.py -method 'MassLength' -hdf5 {}".format(file_) +
                                       " -threshold 'MassLength' -peaks {}".format(args['peaks']) +
                                       " -lmin 3 -readtype {}".format(args['readtype']) + " -centermethod {}".format(args['centers']),
                                       shell=True, stdout=subprocess.PIPE);
    (data,error)    = proc.communicate();

    # Print skipped file if a file is skipped because the pipe comes back empty.
    if data == '':
        skips.append(file_); # append file name data.
        print('Skipped {}'.format(file_))
        iter_counter += 1
    else:
        # Parse data
        length_EXP.append(map(int,re.split('\n+', data)[1].strip('[]').split(',')));
        box_count_EXP.append(map(int,re.split('\n+', data)[3].strip('[]').split(',')));
        mass_EXP.append(map(float,re.split('\n+', data)[5].strip('[]').split(',')));
        sd_EXP.append(map(float,re.split('\n+', data)[7].strip('[]').split(',')))
        fractal_dim_EXP.append(map(float,re.split('\n+', data)[9].strip('[]').split(',')));
        time_step.append(map(float,re.split('\n+', data)[11].strip('[]').split(',')));

        # define all of the variables for the dataset
        length_var      = length_EXP[0][:];
        bc_var          = box_count_EXP[0][:];
        file_var        = np.repeat(file_,len(mass_EXP[0][:]));
        mass_var        = mass_EXP[0][:];
        std_var         = sd_EXP[0][:];
        frac_var        = fractal_dim_EXP[0][:];
        time_var        = np.repeat(time_step[0][:],len(mass_EXP[0][:]));

        # Write a dataset out on a the first iteration.
        if iter_counter == 0:
            # Create the dataset
            dat         = pd.DataFrame({'Filename':file_var,'Length':length_var,'BoxCount':bc_var,'FractalDim':frac_var,'TimeStep':time_var,'Mass':mass_var,'Std':std_var})
            print('First iteration complete on file {}.'.format(file_))
        else:
            # Append the nth dataset with the n-1 dataset
            # Keep appending to the previous dataset.
            dat2        = pd.DataFrame({'Filename':file_var,'Length':length_var,'BoxCount':bc_var,'FractalDim':frac_var,'TimeStep':time_var,'Mass':mass_var,'Std':std_var})
            dat         = dat.append(dat2);
            print('Iteration on file {} complete.'.format(file_))

            if iter_counter + 1 == num_files:
                # Write on the last iteration
                print('All iterations complete.')
                os.chdir('/Volumes/JamesBe/MasterScript/') # change back to the home directory.
                dat.to_csv(write_PATH + "Mach20_1024_MassLength_Periodic_{}".format(args['centers']) + "_N{}".format(args['peaks']) + ".csv");
            elif np.mod(iter_counter + 1,0) == 0:
                dat.to_csv(write_PATH + "Mach20_1024_MassLength_Periodic_{}".format(args['centers']) + "_N{}".format(args['peaks']) + ".csv");


        iter_counter    += 1;
