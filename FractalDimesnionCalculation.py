##############################################################################################################################
import numpy as np;                             # mathematics
import argparse;                                # command line arguments
from skimage import measure;
import imp
import FractalDimensionUtilities
from FractalDimensionUtilities import *         # FractalDimensionUtilities module written by me, for me.
imp.reload(FractalDimensionUtilities)

##############################################################################################################################

# last edited:
# 1 May, 2018: changed the sampling to be linear
# 5 June, 2018: May have found a bug in the boundary conditions.


ap 			= argparse.ArgumentParser(description = 'Just a bunch of input arguments');
ap.add_argument('-video','--video', required=False, help = 'specfiy the mp4 file');
ap.add_argument('-hdf5','--hdf5', required=False, help = 'specfiy the hdf5 file');
ap.add_argument('-threshold', '--threshold',required=True, help = 'threshold argument', type=str);
ap.add_argument('-peaks', '--peaks',required=False, help = 'the total number of peaks in the sim', type=int);
ap.add_argument('-lmin', '--lmin',required=False, help = 'the intial length of the box', type=int);
ap.add_argument('-samples','--samples',required=False,help='the total number of samples through the spatial scale: 3 pixels to image size',type=int);
ap.add_argument('-readtype', '--readtype',required=True, help = 'the read type: all or just velocity', type=str);
ap.add_argument('-method','--method',required=True,help='this selects the fractal dimension method to use.',type=str);
ap.add_argument('-centermethod','--centermethod',required=True,help='this is the method for calculating the centers',type=str);
args 		= vars(ap.parse_args());


if args['method'] == 'MassLength':
    # print 'MassLength'
    # Working Structure
    ##############################################################################################################################
    ###### ANIMATION #####
    dxdy            = args['lmin'];     # the length of the square
    lmin            = args['lmin']      # store the minimum l for sampling
    n               = args['samples']   # the total number of samples
    dxdy_arr        = [];               # store the dxdy values
    log_dxdy_arr    = [];               # store log values for plotting
    log_mass_arr    = [];               # store the mass.
    dxdy_arr        = [];
    mass_arr        = [];
    std_arr         = [];               # standard deviation array.
    box_count       = [];
    fractal_dim     = [];
    iter_count      = 0;                # just a count of the iterations in the while loop


    # Make sure that dxdy is odd.
    if np.mod(dxdy,2) == 0:
         dxdy += 1;

    # Read in data and threshold appropriately
    mask_3d, mask, image, image_thres, time = read_and_threshold(args['video'],args['hdf5'],args['readtype'],args['threshold']);
    lmax                                    = image.shape[1];   # the maximum size of the squares for the counting method

    if args['video'] is not None:
        masked_image    = image*mask_3d;
    else:
        masked_image    = image*mask;

    ## CALCUATE CENTROIDS ##
    centroid_x, centroid_y, max_peak  = centroid_coordinates(mask,image,args['peaks'],args['centermethod']);


    while dxdy < lmax:
        x                   = 0;                                            # initialise the x coordinate
        y                   = 0;                                            # initialise the y coordinate
        num_boxes           = len(centroid_x);                              # total number of boxes equals total amount of centroids
        box_counter         = 0;                                            # initialise the box counter
        Mass_region         = [];


        # for the total amount of squares
        for i in xrange(0,num_boxes):

            # calcuate where the median of the odd dxdy is to center the box on the centroid.
            x = int(centroid_x[i] - ((dxdy-1)/2 + 1));
            y = int(centroid_y[i] - ((dxdy-1)/2 + 1));

            ####################################################
            # Conditions on x for periodic boundary conditions
            ####################################################

            # if the new length from the coordinate is greater than the image width, i.e. the RHS
            # We have to split the sum over the pixel values up across the two rectangular regions.


            if x + dxdy > image.shape[0] and y >= 0 and x >= 0 and y + dxdy <= image.shape[1]:
                #print('x + dxdy > img.shape[0]')
                Mass_Psum_1    = sum(sum(image[y:y+dxdy,x:image.shape[0]]));

                # the partial sum over the second rectangular region
                Mass_Psum_2    = sum(sum(image[y:y+dxdy,0:(dxdy-(image.shape[0]-x))]));

                Mass_sum       = Mass_Psum_1 + Mass_Psum_2;


            # if the coordinate passes through the LHS
            elif x < 0 and y >= 0 and y + dxdy <= image.shape[1] and x + dxdy <= image.shape[0]:
                #print('x < 0')
                Mass_Psum_1    = sum(sum(image[y:y+dxdy,np.mod(x,image.shape[0]):image.shape[0]]));

                delx           = (dxdy-(image.shape[0]-np.mod(x,image.shape[0])))
                Mass_Psum_2    = sum(sum(image[y:y+dxdy,0:delx]));

                Mass_sum       = Mass_Psum_1 + Mass_Psum_2;


            # if the y or the y + dxdy is outside of the image, ignore it.
            elif y + dxdy > image.shape[1] and x >= 0 and y >= 0 and x + dxdy <= image.shape[0]:
                #print('y + dxdy > img.shape[1]')
                # the partial sum over the first rectangular region
                Mass_Psum_1    = sum(sum(image[y:image.shape[1],x:x+dxdy]));

                # the partial sum over the second rectangular region
                Mass_Psum_2    = sum(sum(image[0:(dxdy-(image.shape[1]-y)),x:x+dxdy]));

                Mass_sum       = Mass_Psum_1 + Mass_Psum_2;


            elif y < 0 and x >= 0 and y + dxdy <= image.shape[1] and x + dxdy <= image.shape[0]:
                #print('y < 0')
                # the partial sum over the first rectangular region
                Mass_Psum_1    = sum(sum(image[0:(dxdy-(image.shape[1]-np.mod(y,image.shape[1]))),x:x+dxdy]));

                # the partial sum over the second rectangular region
                Mass_Psum_2    = sum(sum(image[(image.shape[1]+y):image.shape[1],x:x+dxdy]));

                Mass_sum       = Mass_Psum_1 + Mass_Psum_2;

            elif y < 0 and x < 0:
                #print('y < 0 and x < 0')

                yP1   = 0;
                dyP1  = dxdy - (image.shape[1] - np.mod(y,image.shape[1]));

                xP1   = 0;
                dxP1  = dxdy - (image.shape[0] - np.mod(x,image.shape[0]));

                # the partial sum over the first rectangular region
                Mass_Psum_1    = sum(sum(image[yP1:dyP1,xP1:dxP1]));


                yP2   = 0;
                dyP2  = dxdy - (image.shape[1] - np.mod(y,image.shape[1]));

                xP2   = np.mod(x,image.shape[0]);
                dxP2  = image.shape[0];
                # the partial sum over the second rectangular region
                Mass_Psum_2    = sum(sum(image[yP2:dyP2,xP2:dxP2]));

                # the partial sum over the second rectangular region
                Mass_Psum_3    = sum(sum(image[np.mod(y,image.shape[1]):image.shape[1],0:dxP1]));

                # the partial sum over the second rectangular region
                Mass_Psum_4    = sum(sum(image[np.mod(y,image.shape[1]):image.shape[1],np.mod(x,image.shape[0]):image.shape[0]]));

                Mass_sum       = Mass_Psum_1 + Mass_Psum_2 + Mass_Psum_3 + Mass_Psum_4;


            elif y + dxdy > image.shape[1] and x + dxdy > image.shape[0]:
                #print('y + dxdy > img.shape[1] and x + dxdy > img.shape[0]')
                # the partial sum over the first rectangular region
                Mass_Psum_1    = sum(sum(image[y:image.shape[1],x:image.shape[0]]));

                # the partial sum over the second rectangular region
                Mass_Psum_2    = sum(sum(image[y:image.shape[1],0:(dxdy-(image.shape[0]-x))]));

                # the partial sum over the second rectangular region
                Mass_Psum_3    = sum(sum(image[0:(dxdy-(image.shape[1]-y)),0:(dxdy-(image.shape[0]-x))]));

                # the partial sum over the second rectangular region
                Mass_Psum_4    = sum(sum(image[0:(dxdy-(image.shape[1]-y)),x:image.shape[0]]));

                Mass_sum       = Mass_Psum_1 + Mass_Psum_2 + Mass_Psum_3 + Mass_Psum_4;


            elif y+dxdy > image.shape[1] and x < 0:
                #print('y + dxdy > img.shape[1] and x < 0')
                Mass_Psum_1    = sum(sum(image[y:image.shape[1],np.mod(x,image.shape[0]):image.shape[0]]));

                delx           = (dxdy-(image.shape[0]-np.mod(x,image.shape[0])))
                Mass_Psum_2    = sum(sum(image[y:y+image.shape[1],0:delx]));
                # the partial sum over the second rectangular region
                Mass_Psum_3    = sum(sum(image[0:dxdy-(image.shape[1]-y),0:delx]));
                # the partial sum over the second rectangular region
                Mass_Psum_4    = sum(sum(image[0:dxdy-(image.shape[1]-y),np.mod(x,image.shape[0]):image.shape[0]]));

                Mass_sum       = Mass_Psum_1 + Mass_Psum_2 + Mass_Psum_3 + Mass_Psum_4;

            elif x+dxdy > image.shape[0] and y < 0:
                #print('x + dxdy > img.shape[0] and y < 0')

                Mass_Psum_1    = sum(sum(image[0:(dxdy - (image.shape[1] - np.mod(y,image.shape[1]))),x:image.shape[0]]));

                Mass_Psum_2    = sum(sum(image[0:(dxdy - (image.shape[1] - np.mod(y,image.shape[1]))),0:(dxdy-(image.shape[0]-x))]));

                Mass_Psum_3    = sum(sum(image[np.mod(y,image.shape[1]):image.shape[1],x:image.shape[0]]));

                Mass_Psum_4    = sum(sum(image[np.mod(y,image.shape[1]):image.shape[1],0:(dxdy-(image.shape[0]-x))]));

                Mass_sum       = Mass_Psum_1 + Mass_Psum_2 + Mass_Psum_3 + Mass_Psum_4;

            else:
                Mass_sum    = sum(sum(image[y:y+dxdy,x:x+dxdy]));

            box_counter += 1;
            Mass_region.append(Mass_sum);

        # Calculate the mean over each of the squares
        Mass_mean  = np.mean(Mass_region);
        Mass_std   = np.std(Mass_region);

        # Store the amount of boxes and the size of the boxes

        log_mass_arr.append(np.log10(Mass_mean));
        log_dxdy_arr.append(np.log10(dxdy));


        # After the first iteration calculate the linear regression and fractal dimension
        if iter_count >= 1:
            #print('Starting regression on iteration {}'.format(iter_count))

            dxdy_arr.append(dxdy);
            mass_arr.append(Mass_mean);
            std_arr.append(Mass_std);
            box_count.append(len(Mass_region));

            slope, intercept, domain, abline_values, log_log_predict, X, Y = linear_regression(log_mass_arr,log_dxdy_arr,iter_count);

            # Storing all of the predictions
            if iter_count == 1:
                predict_storage = np.array(abline_values);
            else:
                predict_storage = np.vstack([predict_storage,abline_values]);

            # Store the fractal dimension
            fractal_dim.append(slope);

        dxdy             += 2; # sample uniformly in log space.
        #print dxdy
        # Make sure that dxdy is always odd for symmetry about the centroid
        if np.mod(dxdy,2) == 0:
            dxdy += 1

        # Piping this off to the subprocess call in ExperimentalInterface.py
        if dxdy >= lmax:
            print('length:')
            print(dxdy_arr)
            print('\n')
            print('box_count:')
            print(box_count)
            print('\n')
            print('mass:')
            print(mass_arr)
            print('\n')
            print('std:')
            print(std_arr)
            print('\n')
            print('fractal_dim:')
            print(fractal_dim)
            print('\n')
            print('time')
            print(time)
            print('\n')
            print('max_peak:')
            print(max_peak)

        iter_count += 1;


elif args['method'] == 'Boxcounting':
    # Initialisations
    fractal_dim         = [];               # fractal dimension array
    dxdy                = args['lmax'];     # the length of the square
    dxdy_arr            = [];               # store the dxdy values
    log_dxdy_arr        = [];
    log_box_count_arr   = [];
    box_count_arr       = [];               # store the box count array
    iter_count          = 0;                # just a count of the iterations in the while loop
    lmin                = args['lmin'];     # the minimum size of the squares for the counting method

    # Read in data and threshold appropriately
    mask_3d, mask, image, image_thres, time = read_and_threshold(args['video'],args['hdf5'],args['readtype'],args['threshold']);

    # If it is a video apply the 3D mask, if it isn't apply the 1D mask
    if args['video'] is not None:
        masked_image    = image*mask_3d;
    else:
        masked_image    = image*mask

    # while the size of the square is larger than the smallest size square (7 pixels in length)
    while dxdy > lmin:
        # Initialising all of the box parameters (starting vertice and counters)
        x           = 0;                                                # initialise the x coordinate
        y           = 0;                                                # initialise the y coordinate
        num_boxes   = np.int(image.shape[0]*image.shape[1]/dxdy**2);    # an estimate of the total amount of boxes for iteration
        box_counter = 0;                                                # initialise the box counter
        no_box      = 0;

        # for the total amount of squares
        for i in xrange(0,num_boxes):

            # add a rectangle to the plot
            if np.any(mask[y:(y+dxdy+1),x:(x+dxdy+1)]) == True:
                box_counter += 1;
            else:
                no_box += 1;

            # Update x,y coordinates
            x = x + dxdy + 1 ;

            # change y if we step outside of x
            if x + dxdy + 1 > image.shape[1]:
                x           = 0;
                y           = y + dxdy + 1;

            # break if we step outside of y
            if y + dxdy + 1 > image.shape[0]:
                break;

        # Store the amount of boxes and the size of the boxes

        log_box_count_arr.append(np.log(box_counter));
        log_dxdy_arr.append(np.log(dxdy));

        #print('The number of no boxes: {}'.format(no_box))

        # After the first iteration calculate the linear regression and fractal dimension
        if iter_count >= 1:
            box_count_arr.append(box_counter);
            dxdy_arr.append(dxdy);


            #print('Regression on iteration {}'.format(iter_count))
            slope, intercept, domain, abline_values, log_log_predict,X,Y = linear_regression(log_box_count_arr,log_dxdy_arr,iter_count);

            # Storing all of the predictions
            if iter_count == 1:
                predict_storage = np.array(abline_values);
            else:
                predict_storage = np.vstack([predict_storage,abline_values]);

            # Store the fractal dimension
            fractal_dim.append(np.abs(slope));
            fractal_mean        = np.mean(fractal_dim);



        dxdy        = int(dxdy*0.95); # update the size of the square to be 80% of the original

        # Piping this off to the subprocess call in ExperimentalInterface.py
        if dxdy <= lmin:
            print('length:')
            print(dxdy_arr)
            print('\n')
            print('box_count:')
            print(box_count_arr)
            print('\n')
            print('fractal_dim:')
            print(fractal_dim)
            print('\n')
            print('time:')
            print(time)


        iter_count  = iter_count + 1;

elif args['method'] == 'AreaPerimeter':
     mask_3d, mask, image, image_thres, time = read_and_threshold(args['video'],args['hdf5'],args['readtype'],args['threshold']);

     if args['video'] is not None:
         all_labels = label_creator(mask_inv);
     else:
         all_labels = label_creator(mask);

     region_perimeter    = [];
     region_area         = [];
     iter_count = 0;

     for region in measure.regionprops(all_labels):

         # skip small images
         if region.area < 15:
             continue

         # Add to the area vector.
         region_area.append(region.area)

         # Add to the perimeter vector.
         region_perimeter.append(region.perimeter)


     area_mean       = np.mean(region_area);
     area_std        = np.std(region_area);
     perimeter_mean  = np.mean(region_perimeter);
     perimeter_std   = np.std(region_perimeter);

     slope, intercept, domain, abline_values, log_log_predict, X, Y = linear_regression(np.log(region_perimeter),np.log(region_area),iter_count);

     # Piping this off to the subprocess call in ExperimentalInterface.py
     print('Area:')
     print(region_area)
     print('\n')
     print('Perimeter:')
     print(region_perimeter)
     print('\n')
     print('fractal_dim:')
     print(2*slope)
     print('\n')
     print('time')
     print(time)
else:
    print("Please either pick 'BoxCounting', 'AreaPerimeter' or 'MassLength'. ")
