## This is the code accompanying the manuscript: The Mach Number dependence of the fractal dimension in 2D projected density fields driven by supersonic turbulence.

There are three files:

1. ExperimentInterface.py
2. FractalDimensionCalculation.py
3. FractalDimensionUtilities

More supporting documentation will be provided in time. 

